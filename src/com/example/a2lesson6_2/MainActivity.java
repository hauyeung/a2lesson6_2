package com.example.a2lesson6_2;

import android.os.Bundle;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	private Button b;
	private int notificationID = 1;
	private NotificationManager mNotificationManager;
	private PendingIntent pi;
	private Intent notificationIntent;
	private boolean notified = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		b = (Button) findViewById(R.id.nbutton);
		b.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub				
				makenotification();
				
			}
			
		});
		
		findViewById(R.id.rbutton).setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				removenotifications();
				
			}
		});
		
			
		
	}
		
		
	private void makenotification()
	{
		notificationIntent = new Intent();
	    pi = TaskStackBuilder.create(this)
	    	      .addParentStack(MainActivity.class)
	    	      .addNextIntent(notificationIntent)
	    	      .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
	    
		Notification mBuilder = new NotificationCompat.Builder(this)
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentTitle("Notified")
			.setContentText("Notified")			
			.setContentIntent(pi)			
			.build();
		mNotificationManager =
			    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(notificationID, mBuilder);
		notified = true;

	}
	
	private void removenotifications()
	{		
		if (notified)
		{
			mNotificationManager.cancelAll();
			notified = false;
		}
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
